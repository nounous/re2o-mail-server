## Re2o - Switchs config

This service uses Re2o API to generate Swicths config files

More info on https://gitlab.federez.net/re2o/switchs/wikis/home

## Requirements

* python3
* python3-jinja2
* python3-json
* requirements in https://gitlab.federez.net/re2o/re2oapi

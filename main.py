#!/usr/bin/env python3
from configparser import ConfigParser
import socket

from re2oapi import Re2oAPIClient

from jinja2 import Environment, FileSystemLoader

import requests
import base64
import json
from subprocess import call
import os.path

import sys

config = ConfigParser()
config.read('config.ini')

api_hostname = config.get('Re2o', 'hostname')
api_password = config.get('Re2o', 'password')
api_username = config.get('Re2o', 'username')

api_client = Re2oAPIClient(api_hostname, api_username, api_password)

client_hostname = socket.gethostname().split('.', 1)[0]

for arg in sys.argv:
    if arg=="--force":
        generate(api_client)

def generate(api_client):
    all_users = api_client.list("localemail/users")
    # Création de l'environnement Jinja
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('templates/list')
    aliases_rendered = template.render(data=all_users)

    fichier = open('generated/aliases','w')

    if os.path.isfile('aliases_local'): #  if a local aliases file exist, add it's content at the beginning
        local = open('aliases_local','r')
        for line in local.readlines():
            fichier.write(line)
        local.close()

    fichier.write(aliases_rendered)
    fichier.close()

    call(["/usr/bin/newaliases"]) #  Update the aliases config file
    call(["postfix", "reload"]) #  force the reloading now

for service in api_client.list("services/regen/"):
    if service['hostname'] == client_hostname and \
        service['service_name'] == 'mail-server' and \
        service['need_regen']:
        generate(api_client)
        api_client.patch(service['api_url'], data={'need_regen': False})
